#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define VALIDATION_LAYER_COUNT 1

const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;

const char *validationLayers[VALIDATION_LAYER_COUNT] = {"VK_LAYER_KHRONOS_validation"};

#ifdef DEBUG
  const uint8_t enableValidationLayers = 1;
  uint32_t ownExtensions = 1;
#else
  uint32_t ownExtensions = 0;
  const uint8_t enableValidationLayers = 0;
#endif



GLFWwindow* window;
VkInstance instance;
VkDebugUtilsMessengerEXT debugMessenger;
VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
VkDevice device;
VkQueue graphicsQueue;

void run();
void initWindow();
void initVulkan();
void mainLoop();
void cleanup();
void createInstance();
void setupDebugMessenger();
void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT *createInfo);
void pickPhysicalDevice();
uint8_t isDeviceSuitable(VkPhysicalDevice device);
uint32_t findQueueFamilies(VkPhysicalDevice device);
uint8_t checkValidationLayerSupport();

void run(){
  initWindow();
  initVulkan();
  mainLoop();
  cleanup();
}

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                   const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger){
  PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT)
    vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
  if(func != NULL){
    return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
  }else{
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks* pAllocator) {
    PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT)
    vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != NULL) {
        func(instance, debugMessenger, pAllocator);
    }
}



const char ** getRequiredExtensions(uint32_t *extension_count){
  uint32_t glfwExtensionCount = 0;
  const char** glfwExtensions;
  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
  *extension_count = glfwExtensionCount + ownExtensions;
  const char **extensions = malloc((ownExtensions + glfwExtensionCount) * sizeof(const char **));
  printf("%i\n",glfwExtensionCount);
  uint32_t j = 0;
  for(uint32_t i = 0; i < glfwExtensionCount; i++){
    extensions[j] = glfwExtensions[i];
    j++;
  }
  if(enableValidationLayers == 1){
    for(uint32_t i = 0; i < ownExtensions; i++){
      extensions[j] = "VK_EXT_DEBUG_UTILS_EXTENSION_NAME";
        j++;
    }
  }
  printf("%s\n",*extensions);
  printf("%s\n",*(extensions + 1));
  return extensions;
}

uint8_t checkValidationLayerSupport(){
  uint32_t layerCount;
  vkEnumerateInstanceLayerProperties(&layerCount, NULL);
  VkLayerProperties availableLayers[layerCount];
  vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);
  for(uint32_t i = 0; i < VALIDATION_LAYER_COUNT; i++){
    uint8_t layerFound = 0;
    const char* layerName = validationLayers[i];
    for(uint32_t j = 0; j < layerCount; j++){
      if(strcmp(layerName, availableLayers[j].layerName) == 0){
        layerFound = 1;
        break;
      }
    }
    if(layerFound == 0){
      return 0;
    }
  }
  return 1;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData){
  
  printf("validation layer: %s\n", pCallbackData->pMessage);

  return VK_FALSE;
}

void createInstance(){
  if(enableValidationLayers == 1 && checkValidationLayerSupport() == 0){
    printf("Validation layers requested, but not available!\n");
  }

  VkApplicationInfo appInfo;
  appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pApplicationName = "Vulkan C";
  appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
  appInfo.pEngineName = "No Engine";
  appInfo.engineVersion = VK_MAKE_VERSION(1, 0 , 0);
  appInfo.apiVersion = VK_API_VERSION_1_0;

  VkInstanceCreateInfo createInfo;
  createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  createInfo.pApplicationInfo = &appInfo;

  uint32_t extension_count;
  const char ** extensions = getRequiredExtensions(&extension_count);
  if(extension_count > 0){
    printf("Extensions: \n");
  }
  for(int i = 0; i < extension_count; i++){
    printf("  >%s\n",extensions[i]);
  }
  createInfo.enabledExtensionCount = extension_count;
  createInfo.ppEnabledExtensionNames = extensions;

  VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;

  if(enableValidationLayers == 1){
    createInfo.enabledLayerCount = VALIDATION_LAYER_COUNT;
    createInfo.ppEnabledLayerNames = validationLayers;

    populateDebugMessengerCreateInfo(&debugCreateInfo);
    createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *) &debugCreateInfo;
  }else{
    createInfo.enabledLayerCount = 0;

    createInfo.pNext = NULL;
  }

  VkResult result = vkCreateInstance(&createInfo, NULL, &instance);
}

void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT *createInfo){
  createInfo->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  createInfo->messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  createInfo->messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
  createInfo->pfnUserCallback = debugCallback;  
}

void createLogicalDevice(){
  uint32_t indices = findQueueFamilies(physicalDevice);

  VkDeviceQueueCreateInfo queueCreateInfo;
  queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  queueCreateInfo.queueFamilyIndex = indices;
  queueCreateInfo.queueCount = 1;
  printf("indices: %i\n",indices);
  float queuePriority = 1.0f;
  queueCreateInfo.pQueuePriorities = &queuePriority;

  VkPhysicalDeviceFeatures deviceFeatures;

  VkDeviceCreateInfo createInfo;
  createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

  createInfo.pQueueCreateInfos = &queueCreateInfo;
  createInfo.queueCreateInfoCount = 1;

  createInfo.pEnabledFeatures = &deviceFeatures;

  createInfo.enabledExtensionCount = 0;

  if(enableValidationLayers == 1){
    createInfo.enabledLayerCount = VALIDATION_LAYER_COUNT;
    createInfo.ppEnabledLayerNames = validationLayers;
  }else{
    createInfo.enabledLayerCount = 0;
  }

  if(vkCreateDevice(physicalDevice, &createInfo, NULL, &device) != VK_SUCCESS){
    printf("Failed to create logical device!\n");
  }

  vkGetDeviceQueue(device, indices, 0, &graphicsQueue);
}

void initWindow(){
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
  window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", NULL, NULL);
}

void initVulkan(){
  createInstance();
  setupDebugMessenger();
  pickPhysicalDevice();
  createLogicalDevice();
}

void pickPhysicalDevice(){
  uint32_t deviceCount = 0;
  vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
  if(deviceCount == 0){
    printf("Failed to find GPUs with Vulkan support!\n");
  }
  VkPhysicalDevice devices[deviceCount];
  vkEnumeratePhysicalDevices(instance, &deviceCount, devices);
  for(uint32_t i = 0; i < deviceCount; i++){
    if(isDeviceSuitable(devices[i]) == 1){
      physicalDevice = devices[i];
      break;
    }
  }
  if(physicalDevice == VK_NULL_HANDLE){
    printf("Failed to find a suitable GPU!\n");
  }
}

uint8_t isDeviceSuitable(VkPhysicalDevice device){
  uint32_t indices = findQueueFamilies(device);
  if(indices != 0){
    return 1;
  }else{
    return 0;
  }
}

uint32_t findQueueFamilies(VkPhysicalDevice device){
  uint32_t indices = 0;
  uint32_t queueFamilyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, NULL);

  VkQueueFamilyProperties queueFamilies[queueFamilyCount];
  vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies);
  for(int i = 0; i < queueFamilyCount; i++){
    uint32_t tmp = queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT;
    if(tmp != 0){
      indices = 1;
    }
    if(indices != 0){
      break;
    }
  }
  return indices;
}

void setupDebugMessenger(){
  if(enableValidationLayers == 0){
    return;
  }
  VkDebugUtilsMessengerCreateInfoEXT createInfo;
  populateDebugMessengerCreateInfo(&createInfo);
  if(CreateDebugUtilsMessengerEXT(instance, &createInfo, NULL, &debugMessenger) != VK_SUCCESS){
    printf("Failed to set up debug messenger!\n");
  }
}

void mainLoop(){
  while(!glfwWindowShouldClose(window)){
    glfwPollEvents();
  }
}
void cleanup(){
  vkDestroyDevice(device, NULL);
  if(enableValidationLayers == 1){
    vkDestroyDebugUtilsMessengerEXT(instance, debugMessenger, NULL);
  }
  vkDestroyInstance(instance, NULL);
  glfwDestroyWindow(window);
  glfwTerminate();
}

int main(){
  run();
  return 0;
}
