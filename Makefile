CC=gcc
CFLAGS = -std=c99 -O2
LDFLAGS = -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi

vulkan-c: main.c
	$(CC) $(CFLAGS) -o vulkan-c main.c $(LDFLAGS)

.PHONY: test clean

test: vulkan-c
	./vulkan-c

clean:
	rm -f vulkan-c
